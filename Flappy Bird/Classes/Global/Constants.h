//
//  Constants.h
//  MrDentist
//
//  Created by YunXiong Shen on 9/22/13.
//  Copyright (c) 2013 YunXiong Shen. All rights reserved.
//

#ifndef MrDentist_Constants_h
#define MrDentist_Constants_h
#import <RevMobAds/RevMobAds.h>
#import "Chartboost.h"
#import "ALAdDisplayDelegate.h"
#import "ALInterstitialAd.h"

#define OBSTACLE_COUNT 3
#define RATE_ID                     @"J2E8DE2M79"
#define RATE_MESSAGE                @"If you like Candy Bird, please rate it with 5 stars! Thanks!"
#define RATE_TITLE                  @"Candy Bird"

#define LEADERBOARD_ID              @"com.mahatma.candybird"
#define REVMOB_APP_ID               @"52fa853f80d0bbb97d00000f"
#define REVMOB_BANNER_ENABLE        YES
#define CHARTBOOST_APP_ID           @"52fa8604f8975c1fc959790e"
#define CHARTBOOST_APP_SIGNATURE    @"925e179dd8cfa2c031e29897ce23ba68268ea34e"

#define EMAIL_SUBJECT               @"EMAIL SUBJECT"
#define EMAIL_BODY                  @"EMAIL BODY"

#define GAME_FBTEXT                 @"GAME FB TEXT"
#define GAME_TWITTERTEXT            @"GAME Twitter Text"

#define IAP_Q_LIVES                 @"Are you sure you want to get 3 lives and continue game for $0.99?"
#define IAP_LIVES                   @"com.flappybird.lives"

#define DEBUG_MODE                  NO
#define ENABLE_LIVES                YES
#define LIVES_COUNT                 1


#endif
